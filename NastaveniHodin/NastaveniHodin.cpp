#include <Windows.h>
#include <iostream>

DWORD AdjustPrivs(HANDLE hToken, PTOKEN_PRIVILEGES pPrivs, DWORD cbPrivs)
{
	DWORD	i;
	LUID	luidtime;
	BOOL	bSuccess = FALSE;

	LookupPrivilegeValue(NULL, SE_SYSTEMTIME_NAME, &luidtime);

	for (i = 0; i<pPrivs->PrivilegeCount; ++i)
	{
		if (pPrivs->Privileges[i].Luid.HighPart == luidtime.HighPart && pPrivs->Privileges[i].Luid.LowPart == luidtime.LowPart)
		{
			
		}
		else
		{
			pPrivs->Privileges[i].Attributes = SE_PRIVILEGE_REMOVED;
		}
	}

	bSuccess = AdjustTokenPrivileges(hToken, FALSE, pPrivs, cbPrivs, NULL, NULL);

	return bSuccess ? ERROR_SUCCESS : GetLastError();
}

int main(int argc, char* argv[])
{
	BOOL				bResult = FALSE;
	DWORD				dwError = ERROR_SUCCESS;
	DWORD				cbNeeded = 0;
	HANDLE				hToken = NULL;
	PTOKEN_PRIVILEGES	pPrivs = NULL;

	bResult = OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY | TOKEN_ADJUST_PRIVILEGES, &hToken);
	if (bResult)
	{
		bResult = GetTokenInformation(hToken, TokenPrivileges, NULL, 0, &cbNeeded);
		if (!bResult)
		{
			dwError = GetLastError();
			if (dwError == ERROR_INSUFFICIENT_BUFFER)
			{
				bResult = TRUE;
				dwError = 0;
			}
		}

		if (!dwError)
		{
			pPrivs = (PTOKEN_PRIVILEGES)malloc(cbNeeded);

			if (pPrivs)
			{
				bResult = GetTokenInformation(hToken, TokenPrivileges, (LPVOID)pPrivs, cbNeeded, &cbNeeded);

				if (bResult)
				{
					int result = AdjustPrivs(hToken, pPrivs, cbNeeded);
					std::cout << result;
				}

				free(pPrivs);
			}
		}

		CloseHandle(hToken);
	}
	if (argc < 7)
		return -1;
	SYSTEMTIME sys;
	GetSystemTime(&sys);

	sys.wYear = atoi(argv[1]);
	sys.wMonth = atoi(argv[2]);
	sys.wDay = atoi(argv[3]);
	sys.wHour = atoi(argv[4]);
	sys.wMinute = atoi(argv[5]);
	sys.wSecond = atoi(argv[6]);

	BOOL val = SetSystemTime(&sys);

	return 0;
}

