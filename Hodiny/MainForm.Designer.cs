﻿namespace Hodiny
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.timeisLabel = new System.Windows.Forms.Label();
            this.timeField = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 85);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(134, 46);
            this.button1.TabIndex = 0;
            this.button1.Text = "Nastavit čas";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.TimerTick);
            // 
            // timeisLabel
            // 
            this.timeisLabel.AutoSize = true;
            this.timeisLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.timeisLabel.Location = new System.Drawing.Point(12, 21);
            this.timeisLabel.Name = "timeisLabel";
            this.timeisLabel.Size = new System.Drawing.Size(115, 20);
            this.timeisLabel.TabIndex = 1;
            this.timeisLabel.Text = "Datum a čas:";
            // 
            // timeField
            // 
            this.timeField.AutoSize = true;
            this.timeField.Location = new System.Drawing.Point(146, 21);
            this.timeField.Name = "timeField";
            this.timeField.Size = new System.Drawing.Size(33, 20);
            this.timeField.TabIndex = 2;
            this.timeField.Text = "teď";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 143);
            this.Controls.Add(this.timeField);
            this.Controls.Add(this.timeisLabel);
            this.Controls.Add(this.button1);
            this.Name = "MainForm";
            this.Text = "Hodiny";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CloseForm);
            this.Load += new System.EventHandler(this.MainformLoad);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label timeisLabel;
        private System.Windows.Forms.Label timeField;
    }
}

