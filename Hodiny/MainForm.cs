﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using NastaveniHodin;

namespace Hodiny
{
    public partial class MainForm : Form
    {
        private TimeServer server;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainformLoad(object sender, EventArgs e)
        {
            server = new TimeServer();

            server.Start();
        }

        private void TimerTick(object sender, EventArgs e)
        {
            timeField.Text = DateTime.Now.ToString(CultureInfo.CurrentCulture);
        }

        private void CloseForm(object sender, FormClosedEventArgs e)
        {
            server.Stop();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form form = new TimeSetter();
            form.Show();
        }
    }
}
