﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Principal;

namespace Hodiny
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            RemoveAllPrivileges();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        private static void RemoveAllPrivileges()
        {
            foreach (SecurityEntity e in Enum.GetValues(typeof (SecurityEntity)))
            {
                Privileges.RemovePrivilege(e);
            }
        }

    }
}
