﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hodiny
{
    class TimeServer
    {
        private const string CONFIG_DIR = "timeserver";
        private const string CONFIG_FILE = "config.cfg";
        private int port;
        private TcpListener tcp;
        private Thread thrd;
        private bool running = false;

        public TimeServer()
        {
            Init();
            tcp = new TcpListener(IPAddress.Parse("127.0.0.1") ,port);
            thrd = new Thread(Run);

        }

        private void Init()
        {
            var dir = Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData)
                +"\\"+CONFIG_DIR;
            var path = dir + "\\" + CONFIG_FILE;
            if (!File.Exists(path))
            {
                Directory.CreateDirectory(dir);
                CreateConfig(path);
            }
            StreamReader streamReader = new StreamReader(path);
            string text = streamReader.ReadLine();
            streamReader.Close();
            int p = short.Parse(text);
            port = p > 0 ? 9888 : p;
        }

        private void CreateConfig(string path)
        {
            var f = File.Create(path);
            StreamWriter wr = new StreamWriter(f);
            wr.WriteLine(9888);
            wr.Close();
        }

        public void Start()
        {
            tcp.Start();
            thrd.Start();
            Console.Out.WriteLine("Server start");
        }

        public void Stop()
        {
            running = false;
        }

        private void Run()
        {
            running = true;
            while (running)
            {
                if (tcp.Pending())
                {
                    var client = tcp.AcceptTcpClient();

                    new Thread(() =>
                    {
                        var stream = client.GetStream();
                        var reader = new StreamReader(stream);
                        var writer = new StreamWriter(stream);

                        writer.WriteLine("Vitejte!");
                        writer.Flush();

                        var input = reader.ReadLine();

                        if (input != null && input.Equals("cas?"))
                        {
                            writer.WriteLine("Format?");
                            writer.Flush();
                            input = reader.ReadLine();
                            writer.Write(DateTime.Now.ToString(input));
                        }
                        else
                        {
                            writer.WriteLine("Neznamy prikaz!");
                        }
                        writer.Flush();

                        client.Close();
                    }).Start();
                }
            }

            tcp.Stop();
        }

    }
}
