﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace NastaveniHodin
{
    public partial class TimeSetter : Form
    {
        public TimeSetter()
        {
            InitializeComponent();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            //SYSTEMTIME systime = new SYSTEMTIME();
            DateTime dt = dateTimePicker.Value;
            DateTime tm = timePicker.Value;

            Process proc = new Process();
            proc.StartInfo.FileName = "NastaveniHodin.exe";
            proc.StartInfo.Arguments = ""+ dt.Year + " " + dt.Month + " " + dt.Day +
                " " + tm.Hour + " " + tm.Minute + " " + tm.Second;
            proc.Start();

            /*systime.wYear = (ushort) dt.Year;
            systime.wMonth = (ushort) dt.Month;
            systime.wDay = (ushort) dt.Day;
            systime.wHour = (ushort) tm.Hour;
            systime.wMinute = (ushort) tm.Minute;
            systime.wSecond = (ushort) tm.Second;
            systime.wMilliseconds = (ushort) tm.Millisecond;*/

           // SetSystemTime(ref systime);

            Close();
        }

        [DllImport("kernel32.dll")]
        private extern static uint SetSystemTime(ref SYSTEMTIME lpSystemTime);

        private struct SYSTEMTIME
        {
            public ushort wYear;
            public ushort wMonth;
            public ushort wDayOfWeek;
            public ushort wDay;
            public ushort wHour;
            public ushort wMinute;
            public ushort wSecond;
            public ushort wMilliseconds;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
